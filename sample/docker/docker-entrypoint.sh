#!/bin/sh
set -e

echo "Starting liferay"
nohup /srv/liferay/tomcat/bin/catalina.sh run &

tail -f /dev/null
